# Instalar aplicaciones

Desde el panel de control podrás instalar todas las aplicaciones disponibles en MaadiX.  Puedes consultar el listado desde la página '**Instalar aplicacione**'  

![Ver aplicationes](img/es/view-applications.png)  


# Desactivar aplicaciones   

Las aplicaciones pueden ser desactivadas o reactivadas desde la página '**Mis Aplicaciones** -> **Ver todas**'.  
Cuando desactivas una aplicación los datos no se borran, así que si la vuelves a activar en otro momento recuperarás la configuración anterior.  
