# Papelera

En la pestaña 'Papelera' podrás encontrar un listado de los dominios y cuentas que hayas eliminado. Aquí podrás eliminar sus directorios asociados definitivamente seleccionando las casillas 'eliminar' y dándole al botón 'Aplicar cambios'.

![](img/trash.png)

## Dominios

Cuando se elimina un dominio desde el panel de control lo que sucede en el servidor es lo siguiente:

1. Se eliminan el _Virtual Host_ y la renovación de certificados de Let's Encrypt, para que el dominio ya no sea accesible.
2. Se mueve el directorios `/var/www/html/example.com` a `/home/.trash/domains/example.com-timestamp`, para que el contenido de la página o aplicación web pueda ser recuperado en el caso de que lo necesitas.

Sin embargo, por seguridad, una vez borrado un dominio, el contenido de `/var/www/html/example.com` solo puede ser restaurado entrando por terminal con la cuenta Superusuari- (admin). A el directorio `/home/.trash/domains/example.com-timestamp` se le asignará como owner `nobody:nogroup` y solo 'admin' tendrá permisos para modificarlo.

Si quieres eliminar definitivamente el directorio `/home/.trash/domains/example.com-timestamp` puedes marcar 'eliminar' y 'Aplicar cambios'.

## Cuentas

Cuando se elimina una cuenta desde el panel de control lo que sucede en el servidor es que el directorio `/home/user_example` se mueve a `/home/.trash/users/user_example-timestamp`, además se le asignará como owner. `nobody:nogroup`.

Por cuestiones de seguridad, si se quiere recuperar el contenido de este directorio se tendrá que entrar por terminal, con la cuenta Superusuari- (admin) para moverlo de ubicación y cambiarle los permisos de owner.

Si quieres eliminar definitivamente el directorio `/home/.trash/users/user_example-timestamp` puedes marcar 'eliminar' y 'Aplicar cambios'.
