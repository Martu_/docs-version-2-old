# Email

Puedes añadir infinitas cuentas de correo para todos los dominios que tengas activados en tu servidor.  
Antes de añadir cuentas de correo electrónico, es importante que te asegures de haber configurado correctamente el dominio correspondiente. Para saberlo, consulta [Configuración de dominio](/dominios).

# Crear cuenta

En la sección 'Correos' -> 'Cuentas de Correo' , haz clic en el botón 'Añade una cuenta nueva' y rellena los diferentes campos del formulario que se despliega. En el campo 'Usuario' tendrás que insertar solo el nombre de la cuenta (sin @dominio.com) y tendrás que elegir un dominio del listado de dominios disponibles, el cual habrás activado previamente en la sección 'Dominios'.  
Para crear la cuenta 'user3@example.com' tendrás que inesrtar user3 en el campo 'Uusario' y elegir el dominio 'example.com' desde el desplegable del campo 'Dominio'.  

![Screenshot](img/es/mails/add-mail.png)

# Editar cuenta 

Puedes cambiar los parámetros de las cuentas de correo electrónico que tengas activadas visitado la sección **'Correos'** -> **'Cuentas de correo'**  y haciendo luego clic en el botón **'Editar cuenta'** corresponidente a la cuenta que desees modificar.


![Edit email](img/es/mails/edit-email-link.png)

# Reenvío automático (Forward) y respuesta automática (Vacation)

En la misma página de edición de cada cuenta de correo electrónico puedes activar el reenvío automático de todo el correo entrante a cualquier otra dirección de correo.  

Para hacerlo, tienes que activar la casilla **Activar reenvío automático**, insertando una dirección válida en el nuevo campo que se despliega. En caso de que quieras que se reenvie a múltiples cuentas, debes separar cada una con coma (user1@example.com,user2@example.com). Si quieres seguir recibiendo una copia de los correos entrantes en tu cuenta actual, tendrás que incluirla en el listado.

Para activar la respuesta automática (fuera de oficina) activa la casilla **Activar respuesta automática'**. En el acmpo inferior **'Mensaje de respuesta atumática'** inserta el texto que deseas enviar.  


![Forward email](img/es/mails/edit-mail.png)

# Eliminar cuenta

Cuando eliminas una cuenta de correo, los mensajes de sus carpetas (recibidos, enviados, papelera...) no se borran y se guarda una copia en tu servidor. La cuenta es desactivada, de modo que no podrá seguir enviando o recibiendo correo. Si en un futuro vuelves a crear la misma cuenta, recuperarás todo el contenido de sus carpetas.
Para vaciar definitivamente una cuenta de correo, puedes borrar su contenido utilizando la interfaz webmail (si tienes la aplicación instalada) o utilizando un cliente de correo electrónico (Thunderbird, Outlook...) configurada con IMAP. IMAP crea una sincronización entre el cliente y el servidor, de manera que todas las acciones efectuadas en una de las dos partes se refleja en la otra.

# Webmail - Rainloop

Si has instalado la aplicación Rainloop en tu servidor, podrás utilizar esta herramienta webmail para consultar y enviar correo electrónico desde tu navegador.  
Desde el panel de control mismo tienes un acceso directo a la aplicación webmail. Además de poder utilizar la dirección tusubdominio.maadix.org/rainloop, también puedes utilizar cualquier dominio propio que tengas funcionando en el servidor.  
Si, por ejemplo, has configurado con éxito el dominio example.com para tu servidor, puedes visitar la interfaz webmail visitando https://example.com/rainloop     

Todas las cuentas de correo electrónico activadas correctamente a través del panel de control pueden ser consultadas a través de la interfaz webmail.  

# Cliente de correo  

MaadiX permite que puedas consultar tu correo electrónico utilizando un cliente de correo que tengas instalado en tu ordenador (Thunderbird, Outlook...).
Con tal de poder configurar tu cuenta dentro del cliente, necesitas los datos de conexión al servidor, que puedes encontrar en la misma página **'Editar cuenta'** haciendo click en el botón IMAP o POP3 arriba a la derecha..  
